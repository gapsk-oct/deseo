package com.dinero.deseo;

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.lifecycle.viewModelScope
import com.android.basekt.viewmodel.BaseViewModel
import com.dinero.pagecontents.activity.LoginActivity
import com.dinero.pagecontents.activity.MainActivity
import com.dinero.pagecontents.util.UserInfoUtils
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashViewModel : BaseViewModel() {
    var mContex: Context? = null
    override fun onCreat() {
        super.onCreat()
        Log.i("SplashViewModel", "onCreat:==>")
    }

    fun jump() {
        viewModelScope.launch {
            delay(1000)
            UserInfoUtils.mSerionId = UserInfoUtils.getSerionId()
            UserInfoUtils.mPhoneNum = UserInfoUtils.getPhone()

            Log.i(
                "SplashViewModel",
                "delay:==>${UserInfoUtils.mSerionId},${UserInfoUtils.mPhoneNum}"
            )

            var intent = Intent(mContex, LoginActivity::class.java)
            if (UserInfoUtils.mSerionId.isNotEmpty()) {
                intent = Intent(mContex, MainActivity::class.java)
            }
            mContex?.startActivity(intent)

            finishView()
        }
    }
}