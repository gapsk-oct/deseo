package com.dinero.deseo;

import android.support.multidex.MultiDexApplication;

import com.blankj.utilcode.util.Utils;

public class MyApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        Utils.init(this);
    }
}
