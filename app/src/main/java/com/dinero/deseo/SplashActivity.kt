package com.dinero.deseo

import android.util.Log
import com.android.basekt.activity.BaseDataBindingActivity
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.RomUtils
import com.dinero.deseo.databinding.ActivitySplashBinding

/**
 *
 *
 *
 *
墨西哥外包Dinero deseo
APP展示名字：Dinero deseo
包名 ： com.dinero.deseo
修改包名： com.dinero.deseo
产品名：dinero_deseo
渠道名：dinero-deseo
Scheme：dinero://deseo.dinero.com

 *
 */
class SplashActivity : BaseDataBindingActivity<ActivitySplashBinding, SplashViewModel>() {
    override fun initView() {
        mViewModel.mContex = this
        Log.i("SplashViewModel", "initView: ")
    }

    override fun initData() {
        Log.i("SplashViewModel", "initData: ")
        mViewModel.jump()
        try {
            var romInfo = RomUtils.getRomInfo()
            Log.i("CommonParams", "romInfo:===>${romInfo.name}")
        } catch (e: Exception) {
            Log.i("CommonParams", "addCommonParams:===>${e.toString()} ")
        }
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.activity_splash)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<SplashViewModel> {
        return SplashViewModel::class.java
    }

}