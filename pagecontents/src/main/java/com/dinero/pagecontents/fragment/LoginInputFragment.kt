package com.dinero.deseo.fragment

import android.graphics.Color
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.SpanUtils
import com.dinero.pagecontents.R
import com.dinero.pagecontents.base.BaseFragment
import com.dinero.pagecontents.databinding.FragmentLoginInputBinding
import com.dinero.pagecontents.viewmodel.LoginViewModel


class LoginInputFragment : BaseFragment<FragmentLoginInputBinding, LoginViewModel>() {

    override fun initView() {
    }

    override fun initData() {
        mBinding.viewModel = mViewModel

        SpanUtils.with(mBinding.tvTips)
            .append("Para facilitar el contacto, ingrese su ")
            .setSpans()
            .setForegroundColor(Color.parseColor("#ff979797"))
            .append("número celular habitual")
            .setForegroundColor(Color.parseColor("#FFFFDD15"))
            .create()

    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.fragment_login_input)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

}