package com.dinero.pagecontents.fragment

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.basekt.base.BaseDataBindingConfig
import com.dinero.pagecontents.R
import com.dinero.pagecontents.activity.SettingActivity
import com.dinero.pagecontents.adapter.MineAdapter
import com.dinero.pagecontents.base.BaseFragment
import com.dinero.pagecontents.databinding.FragmentMineBinding
import com.dinero.pagecontents.util.UserInfoUtils
import com.dinero.pagecontents.viewmodel.MineViewModel

class MineFragment : BaseFragment<FragmentMineBinding, MineViewModel>() {
    private val mMineAdapter: MineAdapter by lazy {
        MineAdapter().apply {
            addHeaderView(getTopView())
            setOnItemClickListener { _, _, _ ->
                val intent = Intent(context, SettingActivity::class.java);
                startActivity(intent)
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            MineFragment()
    }

    override fun initView() {
    }

    var tv_phone: TextView? = null
    private fun getTopView(): View {
        var view = LayoutInflater.from(context).inflate(R.layout.fragment_mine_top, null)
        tv_phone = view.findViewById(R.id.tv_phone);
        return view
    }

    override fun initData() {

        mBinding.apply {
            viewModel = mViewModel
            recycleView.layoutManager = LinearLayoutManager(context)
            recycleView.adapter = mMineAdapter
        }
        mViewModel.mMineDatas.observe(this) {
            mMineAdapter.setNewInstance(it)
        }
        mViewModel.getMineDatas()

        tv_phone?.let {
            it.text = getMobile()
        }
    }


    fun getMobile(): String {
        var mobile = UserInfoUtils.getPhone()
        Log.i("getMobile", "getMobile: ${mobile}")
        var mobilePsw = ""
        for (i in mobile.indices) {
            if (i < 3 || i > mobile.length - 4) {
                mobilePsw = "${mobilePsw}${mobile[i]}"
            } else {
                mobilePsw = "${mobilePsw}*"
            }
        }
        Log.i("getMobile", "getMobile: $mobilePsw")
        return mobilePsw
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.fragment_mine)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<MineViewModel> {
        return MineViewModel::class.java
    }

}