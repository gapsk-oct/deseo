package com.dinero.pagecontents.fragment

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.SpanUtils
import com.dinero.pagecontents.R
import com.dinero.pagecontents.adapter.HomeAdapter
import com.dinero.pagecontents.base.BaseFragment
import com.dinero.pagecontents.databinding.FragmentHomeBinding
import com.dinero.pagecontents.viewmodel.HomeViewModel

class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>() {


    private var mTopRoot: ConstraintLayout? = null

    private val mHomeAdapter: HomeAdapter by lazy {
        HomeAdapter()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            HomeFragment()
    }


    override fun initView() {
        mViewModel.mHomeDatas.observe(this) {
//            mHomeAdapter.setNewInstance(it)
        }

        mViewModel.mHomeSSKDDatas.observe(this) { data ->
            mHomeAdapter.addHeaderView(getTopView())
            tvTop90?.let {
                it.text = data.particularly
            }
            tvTop120?.let {
                it.text = data.particularly1
            }
            tvTop180?.let {
                it.text = data.particularly2
            }
            tvTopTitle?.let {
                it.text = data.wondered
            }
            tvTopButton?.let {
                it.text = data.successive
            }
            tvTips?.let {
                SpanUtils.with(it)
                    .append("Tasa de Interés ")
                    .setForegroundColor(Color.parseColor("#ff333333"))
                    .append(data.performing)
                    .setForegroundColor(Color.parseColor("#FFA611"))
                    .create()
            }
            tvTopMoney?.let {
                SpanUtils.with(it)
                    .append("$")
                    .setFontSize(18, true)
                    .append(data.nine)
                    .setFontSize(30, true)
                    .create()
            }
        }

        mViewModel.getHomeDatas()
    }


    var tvTips: TextView? = null
    var tvTopTitle: TextView? = null
    var tvTopButton: TextView? = null
    var tvTopMoney: TextView? = null
    var tvTop90: TextView? = null
    var tvTop120: TextView? = null
    var tvTop180: TextView? = null

    private fun getTopView(): View {
        var topView = LayoutInflater.from(context).inflate(R.layout.fragment_home_top_default, null)
        tvTop90 = topView.findViewById(R.id.tv_91)
        tvTop120 = topView.findViewById(R.id.tv_120)
        tvTop180 = topView.findViewById(R.id.tv_180)
        mTopRoot = topView.findViewById(R.id.cl_root)
        tvTopButton = topView.findViewById(R.id.tv_bt)
        tvTopTitle = topView.findViewById(R.id.tv_title)
        tvTips = topView.findViewById(R.id.tv_tips)
        tvTopMoney = topView.findViewById(R.id.tv_money)
        mTopRoot?.setOnClickListener {
            mViewModel.checkProStatus()
        }
        return topView
    }

    override fun initData() {
        mBinding.apply {
            vm = mViewModel
            recycleView.adapter = mHomeAdapter
        }
        mBinding.vm = mViewModel
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.fragment_home)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<HomeViewModel> {
        return HomeViewModel::class.java
    }

}