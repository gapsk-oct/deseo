package com.dinero.pagecontents.fragment

import com.android.basekt.base.BaseDataBindingConfig
import com.dinero.pagecontents.R
import com.dinero.pagecontents.adapter.OrderDetailAdapter
import com.dinero.pagecontents.base.BaseFragment
import com.dinero.pagecontents.databinding.FragmentOrderDetailBinding
import com.dinero.pagecontents.viewmodel.OrderDetailViewModel

class OrderDetailFragment : BaseFragment<FragmentOrderDetailBinding, OrderDetailViewModel>() {

    private val mAdapter: OrderDetailAdapter by lazy {
        OrderDetailAdapter().apply {
            loadMoreModule.isEnableLoadMore = true
            loadMoreModule.setOnLoadMoreListener {
                mViewModel.loadMoreData("4")
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            OrderDetailFragment()
    }

    override fun initView() {
    }


    override fun initData() {
        mBinding.viewModel = mViewModel
        mBinding.recycleView.adapter = mAdapter
        mViewModel.getData("4")
        mViewModel.mOrderDetailDatas.observe(this) {
            if (mAdapter.loadMoreModule.isLoading) {
                mAdapter.loadMoreModule.loadMoreComplete()
            }
            mAdapter.addData(it)
        }
    }


    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.fragment_order_detail)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<OrderDetailViewModel> {
        return OrderDetailViewModel::class.java
    }

}