package com.dinero.pagecontents.fragment

import android.animation.Animator
import android.animation.AnimatorInflater
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.KeyboardUtils
import com.blankj.utilcode.util.ToastUtils
import com.dinero.pagecontents.R
import com.dinero.pagecontents.base.BaseFragment
import com.dinero.pagecontents.databinding.FragmentLoginCodeBinding
import com.dinero.pagecontents.view.PasswordView
import com.dinero.pagecontents.viewmodel.LoginViewModel


class LoginCodeFragment : BaseFragment<FragmentLoginCodeBinding, LoginViewModel>() {

    var mPhoneStr: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.apply {
            getString("phone")?.apply {
                mPhoneStr = this
            }
        }


    }

    override fun initView() {

    }

    override fun initData() {
        mViewModel.mPhoneStr.value = mPhoneStr
        mBinding.viewModel = mViewModel
        mViewModel.isAgree.observe(this) {
            if (mViewModel.isAgree.value == true && mBinding.codePv.password.length == 6) {
                mViewModel.checkCode(mBinding.codePv.password)
            }
        }

        mViewModel.finishActivity.observe(this) {
            Log.i("LoginViewModel", "initData: $it")
            if (it == true) {
                activity?.finish()
            }
        }
        mViewModel.cleanPsw.observe(this) {
            if (it) {
                cleanPswAnimation()
                mBinding.codePv.cleanPassword()
            }
        }

        mBinding.codePv.setPasswordListener(object : PasswordView.PasswordListener {
            /**
             * 输入/删除监听
             *
             * @param changeText  输入/删除的字符
             */
            override fun passwordChange(changeText: String?, password: String?) {
            }

            /**
             * 输入完成
             */
            override fun passwordComplete() {
                activity?.let { KeyboardUtils.hideSoftInput(it) }
                if (mViewModel.isAgree.value == true) {
                    mViewModel.checkCode(mBinding.codePv.password)
                } else {
                    ToastUtils.showShort("Por favor lea y acepte el acuerdo.")
                }
            }

            /**
             * 确认键后的回调
             *
             * @param password   密码
             * @param isComplete 是否达到要求位数
             */
            override fun keyEnterPress(password: String?, isComplete: Boolean) {
            }
        })
        mViewModel.getCode()
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.fragment_login_code)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<LoginViewModel> {
        return LoginViewModel::class.java
    }

    @SuppressLint("ResourceType")
    fun cleanPswAnimation() {
        val animator: Animator =
            AnimatorInflater.loadAnimator(context, R.anim.animation_clean_psw)
        animator.setTarget(mBinding.codePv)
        animator.start()
    }
}