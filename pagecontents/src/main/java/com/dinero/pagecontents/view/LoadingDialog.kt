package com.dinero.pagecontents.view

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import com.dinero.pagecontents.R


class LoadingDialog : Dialog {
    constructor(context: Context) : super(context, R.style.Loading) {
        // 加载布局
        // 加载布局
        setContentView(R.layout.dialog_loading)
        // 设置Dialog参数
        // 设置Dialog参数
        val window: Window? = window

        window?.let {
            val params: WindowManager.LayoutParams = window.getAttributes()
            params.gravity = Gravity.CENTER
            window.setAttributes(params)
        }

    }
}