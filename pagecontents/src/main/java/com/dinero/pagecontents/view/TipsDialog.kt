package com.dinero.pagecontents.view

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.dinero.pagecontents.R


class TipsDialog(
    var title: String = "",
    var titleGravity: Int = Gravity.CENTER,
    var titleColor: Int = Color.parseColor("#ff333333"),
    var tips: String = "",
    var tipsColor: Int = Color.parseColor("#ff999999"),
    var tipsGravity: Int = Gravity.CENTER,
    var left: String = "",
    var right: String = ""
) : DialogFragment() {

    var tvTipsTitle: TextView? = null
    var tvTipsContent: TextView? = null

    var tvTipsLeft: TextView? = null
    var tvTipsRight: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.dialog_tips, container, false)
        tvTipsTitle = view.findViewById<TextView>(R.id.tv_title)

        tvTipsContent = view.findViewById<TextView>(R.id.tv_tips)
        tvTipsLeft = view.findViewById<TextView>(R.id.tv_left)
        tvTipsRight = view.findViewById<TextView>(R.id.tv_right)


        tvTipsTitle?.let {
            it.gravity = titleGravity
            it.setTextColor(titleColor)
        }
        tvTipsContent?.let {
            it.gravity = tipsGravity
            it.setTextColor(tipsColor)
        }

        setTipsTitle(title)
        setTipsContent(tips)
        setTipsLeft(left)
        setTipsRight(right)

        tvTipsLeft?.let {
            it.setOnClickListener {
                dismiss()
                mLeftListener.invoke()
            }
        }
        tvTipsRight?.let {
            it.setOnClickListener {
                dismiss()
                mRightListener.invoke()
            }
        }
        return view
    }

    fun initWidth() {
        // 设置宽度为屏宽、位置在屏幕底部
        // 设置宽度为屏宽、位置在屏幕底部
//        val window: Window = gget()
//        window.setBackgroundDrawableResource(android.R.color.white)
//        window.getDecorView().setPadding(0, 0, 0, 0)
//        val wlp: WindowManager.LayoutParams = window.getAttributes()
//        wlp.gravity = Gravity.BOTTOM
//        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
//        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
//        window.setAttributes(wlp)

    }


    var mLeftListener: () -> Unit = {}

    var mRightListener: () -> Unit = {}


    fun setLeftListener(listener: () -> Unit) {
        mLeftListener = listener
    }

    fun setRightListener(listener: () -> Unit) {
        mRightListener = listener
    }

    fun setTipsLeft(str: String) {
        tvTipsLeft?.let {
            it.text = str
        }
    }

    fun setTipsRight(str: String) {
        tvTipsRight?.let {
            it.text = str
        }
    }

    fun setTipsTitle(str: String) {
        tvTipsTitle?.let {
            it.text = str
        }
    }

    fun setTipsContent(str: String) {
        Log.i("TIPS_DIALOG", "setTipsContent: ${str}")
        tvTipsContent?.let {
            it.text = str
        }
    }
}