package com.dinero.deseo.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.webkit.JavascriptInterface
import com.dinero.pagecontents.activity.WebContainerActivity
import com.dinero.pagecontents.util.PathContants
import com.dinero.pagecontents.util.getRoutePath


class H5Interface(viewTarget: WebContainerActivity) {
    val TAG = "H5Interface"
    val mHandler: Handler by lazy {
        Handler(Looper.getMainLooper())
    }

    var webCotainer: WebContainerActivity = viewTarget

    //回到App首页
    @JavascriptInterface
    fun goIndex() {
        Log.i(TAG, "goIndex: ")
        mHandler.post(Runnable {
            if (webCotainer != null) {
                webCotainer.jumpPage(PathContants.MAIN_ACTIVITY)
                webCotainer.finishView()
            }
        })
    }

    //原生页面跳转
    @JavascriptInterface
    fun nativeJump(linkUrl: String, params: HashMap<String, String>?) {
        Log.i(TAG, "nativeJump: ")
        mHandler.post(Runnable {
            if (webCotainer != null) {
                var bundle = Bundle()
                params?.let {
                    params.forEach {
                        bundle.putString(it.key, it.value)
                    }
                }
                var path = linkUrl.getRoutePath()
                webCotainer.jumpPage(path, bundle)
            }
        })
    }

    //关闭当前H5
    @JavascriptInterface
    fun webClose() {
        Log.i(TAG, "closeSyn: ")
        mHandler.post(Runnable {
            if (webCotainer != null) {
                webCotainer.finishView()
            }
        })

    }

    //    H5页面里的拨打电话
    @JavascriptInterface
    fun phoneCall(phone: String) {
        Log.i(TAG, "phoneCall: ${phone}")
        mHandler.post(Runnable {
            if (webCotainer != null) {
                val dialIntent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone)) //跳转到拨号界面，同时传递电话号码
                webCotainer.startActivity(dialIntent)
            }
        })

    }

    //    顶部右上角拨打电话
    @JavascriptInterface
    fun cornerPhone(phone: String) {
        Log.i(TAG, "cornerPhone: ")
    }

////回到App首页
//    jumpToHome();
////原生页面跳转
//    jump(linkUrl，params);
////关闭当前H5
//    closeSyn()
////H5页面里的拨打电话
//    callPhoneMethod(phone);


//    风控埋点
//    uploadRiskLoan(productId，orderId) =======> deMort(productId，orderId)
//    跳转GooglePlay
//    openGooglePlay(appPkg) =======>    hiGoole(appPkg)
//    跳转Scheme
//    openUrl(url)=======>  schUrl(url)
//    关闭当前H5
//    closeSyn()=======> webClose()
//    原生页面跳转
//    jump(linkUrl，params);=======> nativeJump(linkUrl，params)
//    回到App首页
//    jumpToHome();=======> goIndex()
//    跳登录页
//    jumpToLogin()=======>  goLogin()
//    返回按钮点击信息
//    backDialog(msg)=======> goLastBtn(msg)
//    顶部右上角拨打电话
//    topCallPhone(phone);=======> cornerPhone(phone)
//    H5页面里的拨打电话
//    callPhoneMethod(phone);=======> phoneCall(phone)
//    应用评分
//    toGrade()=======> toSore()
}