package com.dinero.pagecontents.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.dinero.pagecontents.R


class WenDialog(
    var title: String = "",
    var tips: String = "",
    var left: String = "",
    var right: String = ""
) : DialogFragment() {

    var tvTipsTitle: TextView? = null
    var tvTipsContent: TextView? = null

    var tvTipsRight: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialog)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.dialog_wen, container, false)
        tvTipsTitle = view.findViewById<TextView>(R.id.tv_title)
        tvTipsContent = view.findViewById<TextView>(R.id.tv_tips)
        tvTipsRight = view.findViewById<TextView>(R.id.tv_right)


        tvTipsRight?.let {
            it.setOnClickListener {
                dismiss()
            }
        }
        return view
    }

    fun initWidth() {
        // 设置宽度为屏宽、位置在屏幕底部
        // 设置宽度为屏宽、位置在屏幕底部
//        val window: Window = gget()
//        window.setBackgroundDrawableResource(android.R.color.white)
//        window.getDecorView().setPadding(0, 0, 0, 0)
//        val wlp: WindowManager.LayoutParams = window.getAttributes()
//        wlp.gravity = Gravity.BOTTOM
//        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
//        wlp.height = WindowManager.LayoutParams.WRAP_CONTENT
//        window.setAttributes(wlp)

    }


    var mLeftListener: () -> Unit = {}

    var mRightListener: () -> Unit = {}


    fun setLeftListener(listener: () -> Unit) {
        mLeftListener = listener
    }

    fun setRightListener(listener: () -> Unit) {
        mRightListener = listener
    }


    fun setTipsRight(str: String) {
        tvTipsRight?.let {
            it.text = str
        }
    }

    fun setTipsTitle(str: String) {
        tvTipsTitle?.let {
            it.text = str
        }
    }

    fun setTipsContent(str: String) {
        Log.i("TIPS_DIALOG", "setTipsContent: ${str}")
        tvTipsContent?.let {
            it.text = str
        }
    }
}