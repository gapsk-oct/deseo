package com.dinero.pagecontents.util

import com.blankj.utilcode.util.SPUtils

object UserInfoUtils {

    var mSerionId: String = ""

    var mPhoneNum: String = ""

    fun getSerionId(): String {
        var id = SPUtils.getInstance("deseo").getString("SerionId", "")
        mSerionId = id
        return id
    }

    fun getPhone(): String {
        var phone = SPUtils.getInstance("deseo").getString("PhoneNum", "")
        mPhoneNum = phone
        return phone
    }

    fun setSerionId(serssionId: String) {
        mSerionId = serssionId
        SPUtils.getInstance("deseo").put("SerionId", serssionId)
    }

    fun setPhone(phone: String) {
        mPhoneNum = phone
        SPUtils.getInstance("deseo").put("PhoneNum", phone)
    }

    fun cleanAllInfo() {
        setSerionId("")
        setPhone("")
    }

    fun cleanSerssionInfo() {
        setSerionId("")
    }

}