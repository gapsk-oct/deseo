package com.dinero.pagecontents.util

import android.util.Log

object PathContants {

//    setting   设置页（dinero://deseo.dinero.com + /setting）
//    main  首页（scheme + /main）
//    login  登录页（scheme + /login）
//    order 订单列表页 （scheme + /order?tab=1）
//    productDetail   产品详情 （scheme + /productDetail?product_id=1）

    const val LOGIN_ACTIVITY = "login"

    const val MAIN_ACTIVITY = "main"

    const val CERTIFICATION_ACTIVITY = "productDetail"

    const val CERTIFY_PERSON_INFORMATION_ACTIVITY = "certify_person_information_activity"

    const val CERTIFY_WORK_INFORMATION_ACTIVITY = "certify_work_information_activity"

    const val CERTIFY_CONTACT_INFORMATION_ACTIVITY = "certify_contact_information_activity"

    const val CERTIFY_BANK_INFORMATION_ACTIVITY = "certify_bank_information_activity"

    const val WEB_CONTAINER_ACTIVITY = "web_container_activity"

}

fun String.getRoutePath(): String {
    var datas = this.split("/")
    var path = ""
    if (datas.isNotEmpty()) {
        var pathParams = datas[datas.size - 1]
        path = pathParams.split("?")[0]
    }
    Log.i("RoutePath", "getRoutePath:+++>" + path)
    return path
}