package com.dinero.pagecontents.adapter

import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.Fruitful

class CertificationAdapter :
    BaseQuickAdapter<Fruitful, BaseViewHolder>(R.layout.fragment_certification_item) {
    override fun convert(holder: BaseViewHolder, item: Fruitful) {

        var img_flag = holder.getView<ImageView>(R.id.img_flag)
        var img = holder.getView<ImageView>(R.id.img)
        img.setImageResource(R.mipmap.ic_main_1)

        Glide.with(context).load(item.delineation).into(img)
//
        var tv_name = holder.getView<TextView>(R.id.tv_name)
        tv_name.text = item.thanked

        if (item.cotton == 1) {
            img_flag.setImageResource(R.mipmap.ic_certify_complete)
        } else {
            img_flag.setImageResource(R.mipmap.ic_arrow_right)
        }
    }
}