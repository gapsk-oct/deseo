package com.dinero.pagecontents.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.LoadMoreModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.OrderDetailDataBean

class OrderDetailAdapter :
    BaseQuickAdapter<OrderDetailDataBean, BaseViewHolder>(R.layout.fragment_orderdetail_item),
    LoadMoreModule {
    override fun convert(holder: BaseViewHolder, item: OrderDetailDataBean) {
    }
}