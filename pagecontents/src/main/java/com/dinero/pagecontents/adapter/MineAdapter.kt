package com.dinero.pagecontents.adapter

import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.MineDataBean

class MineAdapter :
    BaseQuickAdapter<MineDataBean, BaseViewHolder>(R.layout.fragment_mine_item) {

    override fun convert(holder: BaseViewHolder, item: MineDataBean) {
        var img = holder.getView<ImageView>(R.id.img)
        img.setImageResource(item.icon)

        var name = holder.getView<TextView>(R.id.tv_name)
        name.setText(item.name)
    }

}