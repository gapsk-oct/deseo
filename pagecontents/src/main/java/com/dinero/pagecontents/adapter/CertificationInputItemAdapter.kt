package com.dinero.pagecontents.adapter

import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.Continuing

class CertificationInputItemAdapter :
    BaseQuickAdapter<Continuing, BaseViewHolder>(R.layout.fragment_certification_input_item_txt) {

    var isShowWen = false
    override fun convert(holder: BaseViewHolder, item: Continuing) {
        val ll_other = holder.getView<LinearLayout>(R.id.ll_other)
        val img_wenhao = holder.getView<ImageView>(R.id.img_wenhao)
        val tv_tips = holder.getView<TextView>(R.id.tv_tips)
        val ll_tips = holder.getView<LinearLayout>(R.id.ll_tips)
        val ll_content = holder.getView<LinearLayout>(R.id.ll_content)
        val img_flag = holder.getView<ImageView>(R.id.img_flag)
        val tv_other_value = holder.getView<TextView>(R.id.tv_other_value)
        val tv_txt_value = holder.getView<TextView>(R.id.tv_txt_value)
        val tvName = holder.getView<TextView>(R.id.tv_title)

        if (item.intent.contentEquals("D", true)) {
            ll_tips.visibility = View.VISIBLE
            ll_content.visibility = View.GONE
            tv_tips.text = item.thanked
        } else {
            ll_tips.visibility = View.GONE
            ll_content.visibility = View.VISIBLE

            tvName.text = item.thanked
            if (item.intent == "B" && item.selData != null) {
                tv_other_value.text = item.selData.attended
            } else if (item.intent == "A") {
                tv_txt_value.hint = item.edHint
                tv_txt_value.text = item.howqw
                Log.i("edMaxLenth", "convert: ===>" + item.edMaxLenth)
                tv_txt_value.filters =
                    arrayOf<InputFilter>((InputFilter.LengthFilter(item.edMaxLenth)))
            } else if (item.intent == "F") {
                tv_other_value.text = item.howqw
            }
        }
        tv_txt_value.setFocusable(true);//设置输入框可聚集
        tv_txt_value.setFocusableInTouchMode(true);//设置触摸聚焦

        //A 输入框，否则弹框
        tv_txt_value.visibility = if (item.intent == "A") View.VISIBLE else View.GONE
        ll_other.visibility = if (item.intent == "A") View.GONE else View.VISIBLE
        img_flag.visibility = if (item.intent == "A") View.GONE else View.VISIBLE
        if ("cardNo" == item.between) {
            tv_txt_value.inputType = InputType.TYPE_CLASS_NUMBER
        }
        if ("cardNo" == item.between && isShowWen) {
            img_wenhao.visibility = View.VISIBLE

        } else {
            img_wenhao.visibility = View.GONE
        }

        tv_txt_value.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                Log.i("InputItemAdapter", "afterTextChanged:${s} ")
                if (s.isNullOrEmpty()) {
                    item.howqw = ""
                } else {
                    item.howqw = s.toString()
                }
            }
        })
    }
}