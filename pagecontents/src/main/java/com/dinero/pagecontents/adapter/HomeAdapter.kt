package com.dinero.pagecontents.adapter

import android.view.View
import android.widget.ImageView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.HomeDataBean

class HomeAdapter : BaseQuickAdapter<HomeDataBean, BaseViewHolder>(R.layout.fragment_home_item) {
    override fun convert(holder: BaseViewHolder, item: HomeDataBean) {

        var imgFlag = holder.getView<ImageView>(R.id.img_flag)

        if (holder.layoutPosition == 0) {
            imgFlag.visibility = View.VISIBLE
            imgFlag.setImageResource(R.mipmap.ic_home_1)
        } else if (holder.layoutPosition == 1) {
            imgFlag.visibility = View.VISIBLE
            imgFlag.setImageResource(R.mipmap.ic_home_2)
        } else if (holder.layoutPosition == 2) {
            imgFlag.visibility = View.VISIBLE
            imgFlag.setImageResource(R.mipmap.ic_home_3)
        } else {
            imgFlag.visibility = View.GONE
        }

    }
}