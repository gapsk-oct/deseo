package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class CertifyInformationDataBean(
    val continuing: MutableList<Continuing>
)