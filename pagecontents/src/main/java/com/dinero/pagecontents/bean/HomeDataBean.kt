package com.dinero.pagecontents.bean

import androidx.annotation.Keep


@Keep
data class HomeDataBean(
    val duty: Duty,
    val godf: List<Godf>
)

