package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class MineDataBean(var icon: Int, var name: String)