package com.dinero.pagecontents.bean

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class BaseBean<T>(
    @SerializedName("tell")
    val data: T,
    @SerializedName("between")
    var code: String,
    @SerializedName("endf")
    val message: String
) {
    fun isDataSuccess(): Boolean {
        return "00" == code
    }

}


fun BaseBean<Any>.isDataSuccess(): Boolean {
    return "00" == code
}


fun String.isNetSuccess(): Boolean {
    return "00" == this
}

fun String.isNeedLogin(): Boolean {
    return "-2" == this
}