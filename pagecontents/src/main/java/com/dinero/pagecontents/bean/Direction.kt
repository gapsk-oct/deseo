package com.dinero.pagecontents.bean

import androidx.annotation.Keep

/**
 *  "tisd": "",  //跳转url
"written": null,
"heartily35": 0, //产品id
"thanked": "",
"willing": "https://yinni-files-dev.oss-cn-shanghai.aliyuncs.com/commonpic/kilatbanner.png", //图片url
"held": "1", //排序
"tenth": "fixedbanner",
"severe": null,
"disappointment": null,
"repetition": null
 */
@Keep
data class Direction(
    val tisd: String,
    val heartily35: String,
    val willing: String,
    val overa: String,
    val performing: String,
    val nine: String,
    val wondered: String,
    val successive: String,
    val particularly: String,
    val particularly1: String,
    val particularly2: String,
)