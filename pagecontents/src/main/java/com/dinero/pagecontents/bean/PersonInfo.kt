package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class PersonInfo(
    val alps: String,
    val counties: String,
    val midland: String
)