package com.dinero.pagecontents.bean
import androidx.annotation.Keep

@Keep
data class OrderDataBean(
    val english: Int,
    val godf: List<OrderDetailDataBean>
)