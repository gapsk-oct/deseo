package com.dinero.pagecontents.bean

import android.util.Log
import org.json.JSONException
import retrofit2.HttpException
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException

class ApiResponse<T> {
    var code = SUCCESS_CODE
    var body: T? = null
    var msg: String? = null
    var TAG = "ApiResponse"

    fun await(): T? {
        return body
    }

    constructor(response: Response<T>) {
        Log.i(TAG, "====esponse: Response<T====")
        body = response.body()
    }

    constructor(t: T) {
        Log.i(TAG, "===t: T)====")
        body = t
    }

    constructor(throwable: Throwable) {
        Log.i(TAG, "=== constructor(throwable: Throwable)====")
        body = null
        code = ERROR_CODE

        //对异常进行判断，这个是我随便写的一点，可以写一个工具类给封装起来
        msg = when (throwable) {
            is SocketTimeoutException -> "超时"
            is HttpException -> {
                when (throwable.code()) {
                    404 -> "没有找到合适的资源"
                    500 -> "服务器内部错误"
                    else -> throwable.message()
                }
            }
            is JSONException -> "json解析异常"
            is UnknownHostException -> "网络异常"
            else -> throwable.message
        }
    }

    companion object {
        private const val TAG = "ApiResponse"
        const val ERROR_CODE = 99999
        const val SUCCESS_CODE = 200
        fun error(throwable: Throwable): ApiResponse<*> {
            return ApiResponse<Any?>(throwable)
        }
    }
}