package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class Godf(
    val direction: Direction,
    val oxford: String
)