package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class ProApplyDataBean(
    val beloved: Int,
    val oxford: Int,
    val tisd: String,
    val wifef: String
)