package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class Duty(
    val inclination36: String,
    val knows: String
)