package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class OrderDetailDataBean(
    val appear: String,
//    val bads: String,
//    val believed: String,
//    val conviction: String,
//    val disappointment: Int,
//    val fear: String,
//    val hearts: String,
//    val hereafter: String,
//    val imperfection: String,
//    val mixture: String,
//    val mornings: String,
//    val need: String,
//    val slight: String,
//    val successive: String,
//    val surprised: String,
//    val though: String,
//    val unequal: String,
//    val wondered: String
)