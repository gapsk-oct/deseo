package com.dinero.pagecontents.bean
import androidx.annotation.Keep
import com.contrarywind.interfaces.IPickerViewData

@Keep
data class CertifyInputItemDataBean(
    val continuing: List<Continuing>
)

@Keep
data class Continuing(
    val between: String,//key 提交
    val cotton: String,
    val desire: Boolean,
    val grew: Int,
    var howqw: String = "",
    var edHint: String = "",//输入框hint
    var edMaxLenth: Int ,//输入框最大字符串
    val intent: String,//B选择框，F是生日选择，A是文本输入 ，D提示类型条目
    val isabella: String,
    val matched: List<Matched>,
    val netting: String,
    val overa: String,
    val rooms: Int,
    var thanked: String,
    val world: String,
    var selData: Matched = Matched(),// 选择框选中的数据
)

@Keep
class Matched : IPickerViewData {

    var attended: String = ""

    var oxford: String = ""

    var thanked: String = ""

    override fun getPickerViewText(): String {
        return attended
    }
}
