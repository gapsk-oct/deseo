package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class CertifyContactDataBean(
    val insensible: Insensible
)
@Keep
data class Insensible(
    val acting26: String,//第二联系人关系ID
    val formed: String,
    val future: String,//第二联系人姓名
    val greatest: String,//第一联系人手机号
    val hand: List<Matched>,
    val happier: String,//第一联系人姓名
    val judging: String,//第二联系人手机号
    val lenient27: List<Matched>,
    val sense: String//第一联系人关系ID
)

