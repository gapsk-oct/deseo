package com.dinero.pagecontents.bean

import androidx.annotation.Keep

@Keep
data class CertificationDataBean(
    val beloved: Int,
    val doubt: Doubt,
    val existence: Existence,//产品信息
    val faithful: List<Faithful>,//借款协议
    val fruitful: List<Fruitful>,//认证项
    val pine: Pine
)
@Keep
data class Doubt(
    val attended: String,
    val horrors: String,
    val represented: String
)
@Keep
data class Existence(
    val bads: String,
    val believed: String,
    val beyond: String,
    val central: String,
    val country: Int,
    val extremities: String,
    val fear: Int,
    val northern: Northern,
    val overa: String,
    val ownrr: Ownrr,
    val part: List<String>,
    val security: List<Int>,
    val successive: String,
    val surely: String,
    val tisd: String,
    val western: String,
    val wondered: String
)
@Keep
data class Faithful(
    val fear: Int,
    val forests: String,
    val need: String,
    val thanked: String,
    val vices: Int,
    var noasdax: String,
)
@Keep
data class Fruitful(
    val cotton: Int, //是否已完成
    val delineation: String, //标题图片
    val france: String,
    val italy: String,
    val netting: String,
    val oxford: Int,
    val rooms: Int,
    val south: Int,
    val switzerland: Int,
    val thanked: String,//标题
    val tisd: String,
    val world: String
)
@Keep
data class Pine(
    val oxford: Int,
    val pyrenees: String,
    val thanked: String,
    val tisd: String
)
@Keep
data class Northern(
    val hard: Hard,
    val yielded: Yielded
)
@Keep
data class Ownrr(
    val howqw: String
)
@Keep
data class Hard(
    val pressed: String,
    val thanked: String
)
@Keep
data class Yielded(
    val pressed: String,
    val thanked: String
)