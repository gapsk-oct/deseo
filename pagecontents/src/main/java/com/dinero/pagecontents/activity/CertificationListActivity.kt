package com.dinero.pagecontents.activity

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.BarUtils
import com.blankj.utilcode.util.SpanUtils
import com.blankj.utilcode.util.ToastUtils
import com.dinero.pagecontents.BuildConfig
import com.dinero.pagecontents.adapter.CertificationAdapter
import com.dinero.pagecontents.bean.Faithful
import com.dinero.pagecontents.bean.Fruitful
import com.dinero.pagecontents.util.PathContants
import com.dinero.pagecontents.R
import com.dinero.pagecontents.base.BaseActivity
import com.dinero.pagecontents.databinding.ActivityCertificationListBinding
import com.dinero.pagecontents.viewmodel.CertificationListViewModel

/**
 * @Created on 2021/12/17 15:04
 * @Author
 * @Describe
 */
class CertificationListActivity :
    BaseActivity<ActivityCertificationListBinding, CertificationListViewModel>() {

    private val certificationAdapter: CertificationAdapter by lazy {
        CertificationAdapter().apply {
            setOnItemClickListener { _, _, position ->
                //'credit'=>"pf1", 'public'=>"pf2", 'personal'=>"pf3", 'job'=>"pf4", 'ext'=>"pf5", 'bank'=>"pf6",
                jumpCertify(position)
            }
        }
    }

    var proId: String = ""
    var TAG: String = "CERTYYYYYY"


    /**
     * isJump 是否是跳转
     */
    fun jumpCertify(position: Int, isJump: Boolean = false) {
        var jumpPosition = -1

        if (position >= 0 && certificationAdapter.data[position].cotton == 1) {
            jumpPosition = position
        } else {
            for (index in 0 until certificationAdapter.data.size) {
                if (certificationAdapter.data[index].cotton != 1) {
                    jumpPosition = index
                    break
                } else if (certificationAdapter.data[index].cotton == 1 && index == certificationAdapter.data.size - 1) {
                    jumpPosition = 999
                }
            }
        }
        if (BuildConfig.DEBUG) {
            jumpPosition = position
        }
        Log.i(TAG, "jumpCertify:jumpPosition===>${jumpPosition} ")
        if (isJump && jumpPosition == 999) {
            if (mViewModel.isAgree.value == true) {
                mViewModel.buyPro()
            } else {
                ToastUtils.showShort("Por favor lea y acepte el acuerdo.")
            }

        } else {
            if (jumpPosition != -1) {
                var item = certificationAdapter.data[jumpPosition]
                Log.i(TAG, "jumpCertify:item==>${jumpPosition},${item.thanked} ，${proId}")
                val bundle = Bundle()
                bundle.putString("proId", proId)
                bundle.putString("title", item.thanked)
                bundle.putString("ordId", mViewModel.mCertificationOrderInfo.value)
                bundle.putString("complete", item.cotton.toString())
                when (item.france) {
                    "pf3" -> {
                        jumpPage(
                            PathContants.CERTIFY_PERSON_INFORMATION_ACTIVITY,
                            bundle
                        )
                    }
                    "pf4" -> {
                        jumpPage(
                            PathContants.CERTIFY_WORK_INFORMATION_ACTIVITY,
                            bundle
                        )
                    }
                    "pf5" -> {
                        jumpPage(
                            PathContants.CERTIFY_CONTACT_INFORMATION_ACTIVITY,
                            bundle
                        )
                    }
                    "pf6" -> {
                        jumpPage(
                            PathContants.CERTIFY_BANK_INFORMATION_ACTIVITY,
                            bundle
                        )
                    }
                }
            }

        }

    }

    override fun initView() {

        proId = intent.getStringExtra("proId").toString()
        Log.i(TAG, "jumpCertify:proId==>，${proId}")

        BarUtils.transparentStatusBar(this)
        BarUtils.addMarginTopEqualStatusBarHeight(mBinding.topTitle)

        mBinding.recycleView.adapter = certificationAdapter
        mBinding.tvConfirm.setOnClickListener {
            jumpCertify(-1, true)
        }

        mViewModel.mCertificationDatas.observe(this) {
            var data = mutableListOf<Fruitful>()
            data.addAll(it)
            certificationAdapter.setNewInstance(data)
        }
        mViewModel.mAgreeDatas.observe(this) { datas ->
            var spanUtils = SpanUtils.with(mBinding.tvTip)
            spanUtils.append("He leído y estoy de acuerdo con ")
            datas.forEach {
                spanUtils.append("${it.thanked}")
                    .setClickSpan(Color.BLUE, true, object : View.OnClickListener {
                        override fun onClick(v: View?) {
                            jumpAgree(it)
                        }
                    })
            }
            spanUtils.create()
        }
        mViewModel.mAllDatas.observe(this) {
            mBinding.tvConfirm.text = it.existence.successive
            SpanUtils.with(mBinding.tvMoney)
                .append("$")
                .setFontSize(resources.getDimension(R.dimen.sp_8).toInt(), true)
                .append(it.existence.surely)
                .setFontSize(resources.getDimension(R.dimen.sp_22).toInt(), true)
                .create()
        }

        mBinding.viewModel = mViewModel
    }

    fun jumpAgree(data: Faithful) {
        mViewModel.getAgreeLink(proId, data)
    }

    var isFirstCreat = true

    override fun onResume() {
        super.onResume()
        proId?.let {
            mViewModel.getDatas(it, isFirstCreat)
            isFirstCreat = false
        }
    }

    override fun initData() {
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.activity_certification_list)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<CertificationListViewModel> {
        return CertificationListViewModel::class.java
    }

    override fun isLightMode(): Boolean {
        return false
    }
}