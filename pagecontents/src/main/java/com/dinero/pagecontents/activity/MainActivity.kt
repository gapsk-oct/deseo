package com.dinero.pagecontents.activity

import android.util.Log
import android.view.KeyEvent
import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.BarUtils
import com.blankj.utilcode.util.ToastUtils
import com.dinero.pagecontents.fragment.HomeFragment
import com.dinero.pagecontents.fragment.MineFragment
import com.dinero.pagecontents.viewmodel.MainViewModel
import com.dinero.pagecontents.R
import com.dinero.pagecontents.base.BaseActivity
import com.dinero.pagecontents.databinding.ActivityMainBinding

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    var mHomeFragment: HomeFragment? = null
    var mMineFragment: MineFragment? = null
    override fun initView() {
        mBinding.vm = mViewModel
    }

    override fun initData() {
        BarUtils.transparentStatusBar(this)
        mViewModel.mShowView.observe(this) {
            if (it == 1) {
                showHome()
            } else if (it == 2) {
                showMine()
            }
        }
        mViewModel.mShowView.value = 1
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.activity_main)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<MainViewModel> {
        return MainViewModel::class.java
    }

    private fun showHome() {
        Log.i("TAG", "showHome: ")
        var trans = supportFragmentManager.beginTransaction()

        if (mHomeFragment == null) {
            mHomeFragment = HomeFragment()
            trans.add(R.id.fl_main_container, mHomeFragment!!)
        }

        if (mMineFragment != null) {
            trans.hide(mMineFragment!!)
        }

        trans.show(mHomeFragment!!).commitAllowingStateLoss()

    }

    private fun showMine() {
        Log.i("TAG", "showMine: ")
        var trans = supportFragmentManager.beginTransaction()
        if (mMineFragment == null) {
            mMineFragment = MineFragment.newInstance()
            trans.add(R.id.fl_main_container, mMineFragment!!)
        }

        trans.hide(mHomeFragment!!)

        trans.show(mMineFragment!!).commitAllowingStateLoss()
    }

    var exitTime: Long = 0;
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            exitAfterTwice()
            return false
        }
        return super.onKeyDown(keyCode, event)
    }

    /**
     * 连续点击2次退出
     */
    fun exitAfterTwice() {
        if ((System.currentTimeMillis() - exitTime) > 2000) {
            ToastUtils.showShort("Favor de hacer click en Regresar para salir")
            exitTime = System.currentTimeMillis();
        } else {
            Log.i(TAG, "System: ")
            finish();
            System.exit(0);
        }
    }

    var TAG = "MAINMAIN"
    override fun finishView() {
        super.finishView()
        Log.i(TAG, "finishView: ")
    }
}