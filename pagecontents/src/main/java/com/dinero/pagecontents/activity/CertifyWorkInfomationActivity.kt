package com.dinero.pagecontents.activity

import com.dinero.pagecontents.activity.CertifyPersonInformationActivity

/**
 * @Created on 2021/12/22 19:47
 * @Author  工作信息
 * @Describe
 */
class CertifyWorkInfomationActivity : CertifyPersonInformationActivity() {
    override fun getViewType(): String {
        return "work"
    }
}