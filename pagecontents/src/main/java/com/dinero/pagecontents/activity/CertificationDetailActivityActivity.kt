package com.dinero.pagecontents.activity

import com.dinero.pagecontents.R
import com.android.basekt.activity.BaseDataBindingActivity
import com.android.basekt.base.BaseDataBindingConfig
import com.dinero.pagecontents.databinding.ActivityCertificationDetailBinding
import com.dinero.pagecontents.viewmodel.CertificationDetailActivityViewModel

/**
 * @Created on 2021/12/21 19:41
 * @Author
 * @Describe
 */
class CertificationDetailActivity :
    BaseDataBindingActivity<ActivityCertificationDetailBinding, CertificationDetailActivityViewModel>() {
    override fun initView() {
    }

    override fun initData() {
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.activity_certification_detail)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<CertificationDetailActivityViewModel> {
        return CertificationDetailActivityViewModel::class.java
    }

}