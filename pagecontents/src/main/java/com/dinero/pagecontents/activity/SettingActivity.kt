package com.dinero.pagecontents.activity

import com.android.basekt.base.BaseDataBindingConfig
import com.blankj.utilcode.util.ActivityUtils
import com.dinero.pagecontents.R
import com.dinero.pagecontents.base.BaseActivity
import com.dinero.pagecontents.util.PathContants
import com.dinero.pagecontents.util.UserInfoUtils
import com.dinero.pagecontents.view.TipsDialog
import com.dinero.pagecontents.databinding.ActivitySettingBinding
import com.dinero.pagecontents.viewmodel.SettingViewModel

/**
 * @Created on 2021/12/15 19:53
 * @Author
 * @Describe
 */
class SettingActivity : BaseActivity<ActivitySettingBinding, SettingViewModel>() {
    override fun initView() {
        mBinding.viewModel = mViewModel
    }

    override fun initData() {
        mBinding.btClean.setOnClickListener { clearSeesion() }
    }

    /**
     * 获取binding相关配置
     *
     * @return
     */
    override fun getDataBindingConfig(): BaseDataBindingConfig {
        return BaseDataBindingConfig(R.layout.activity_setting)
    }

    /**
     * 获取viewmodel Clazz
     *
     * @return
     */
    override fun initViewModelClazz(): Class<SettingViewModel> {
        return SettingViewModel::class.java
    }


    val tipsDialog by lazy {
        TipsDialog(
            title = "Cerrar sesión",
            tips = "Está seguro/a de cerrar sesión?",
            left = "Confirmar",
            right = "Cancelar"
        ).apply {
            setLeftListener {
                UserInfoUtils.cleanSerssionInfo()
                jumpPage(PathContants.LOGIN_ACTIVITY)
                ActivityUtils.finishAllActivities()
            }
        }
    }


    fun clearSeesion() {
        tipsDialog.show(supportFragmentManager, "")
    }
}