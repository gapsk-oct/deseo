package com.dinero.pagecontents.viewmodel

import androidx.lifecycle.viewModelScope
import com.android.basekt.viewmodel.BaseViewModel
import com.dinero.pagecontents.net.ApiClient
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class TestViewModel : BaseViewModel() {
    fun getData() {
        viewModelScope.launch {
            var params = hashMapOf<String, String>()
            params.put("device_imei", "123456789")

            var data = async {
                ApiClient.instance.getApiService().login(params)
            }

            var str = data.await()

            str.code
        }
    }

}