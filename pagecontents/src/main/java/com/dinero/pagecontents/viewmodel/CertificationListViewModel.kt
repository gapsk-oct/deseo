package com.dinero.pagecontents.viewmodel;

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.basekt.base.BaseViewEventConstants
import com.android.basekt.base.JumpActivityEvent
import com.android.basekt.viewmodel.BaseViewModel
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.ToastUtils
import com.dinero.pagecontents.net.ApiClient
import com.dinero.pagecontents.util.PathContants
import com.dinero.pagecontents.bean.*
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class CertificationListViewModel : BaseViewModel() {
    val mCertificationDatas = MutableLiveData<List<Fruitful>>()

    val mAgreeDatas = MutableLiveData<List<Faithful>>()

    val mAllDatas = MutableLiveData<CertificationDataBean>()

    //订单号
    val mCertificationOrderInfo = MutableLiveData<String>()

    //金额
    val mCertificationMoneyInfo = MutableLiveData<String>()


    var isAgree = MutableLiveData(true)
    fun agree() {
        isAgree.value = !isAgree.value!!
    }


    fun getDatas(proId: String, isShowLoading: Boolean = true) {

        viewModelScope.launch {
            if (!NetworkUtils.isConnected()) {
                ToastUtils.showShort("No internet")
                return@launch
            }
            try {
                if (isShowLoading) {

                }
                BaseViewEventConstants.SHOW_LOADING_VIEW.publish()

                var detailNet = netApi {
                    var params = hashMapOf<String, String>()
                    params.put("heartily35", proId)
                    ApiClient.instance.getApiService().getProDetail(params)
                }

                var detailBean: BaseBean<CertificationDataBean>? = detailNet.await()
                detailBean?.let {
                    if (detailBean.code.isNetSuccess()) {
                        mAllDatas.value = detailBean.data
                        mCertificationDatas.value = detailBean.data.fruitful
                        mAgreeDatas.value = detailBean.data.faithful
                        mCertificationOrderInfo.value = detailBean.data.existence.western
                        mCertificationMoneyInfo.value = detailBean.data.existence.surely.toString()
                    } else {
                        ToastUtils.showShort(detailBean.message)
                    }
                }

            } catch (e: Exception) {
                Log.i(TAG, "Exception: ")
            }

            delay(150)

            BaseViewEventConstants.DISMISS_VIEW.publish()
        }
    }


    fun getAgreeLink(proId: String, data: Faithful) {

        viewModelScope.launch {
            if (!NetworkUtils.isConnected()) {
                ToastUtils.showShort("No internet")
                return@launch
            }
            BaseViewEventConstants.SHOW_LOADING_VIEW.publish()
            var net = netApi {
                var params = hashMapOf<String, String>()
                params.put("fear", data.fear.toString())
                params.put("forests", data.forests)
                params.put("vices", data.vices.toString())
                params.put("need", data.need)
                ApiClient.instance.getApiService().getAgreeLink(params)
            }
            var detailBean = net.await()
            detailBean?.let {
                BaseViewEventConstants.DISMISS_VIEW.publish()
                Log.i(TAG, "getAgreeLink: ${detailBean}")
                Log.i(TAG, "detailBean.data.noasdax: ${detailBean.data.noasdax}")
                if (detailBean.isDataSuccess() && !detailBean.data.noasdax.isNullOrEmpty()) {
                    Log.i(TAG, "jump: ${detailBean}")
                    var bundle = Bundle()
                    bundle.putString("urlPath", detailBean.data.noasdax)
                    JumpActivityEvent(PathContants.WEB_CONTAINER_ACTIVITY, bundle).publish()
                }
            }
        }
    }

    fun buyPro() {
        BaseViewEventConstants.SHOW_LOADING_VIEW.publish()
        viewModelScope.launch {
            if (!NetworkUtils.isConnected()) {
                ToastUtils.showShort("No internet")
                return@launch
            }

            if (isAgree.value != true) {
                ToastUtils.showShort("Por favor lea y acepte el acuerdo.")
                return@launch
            }

            try {
                var applyNet = netApi {
                    var params = hashMapOf<String, String>()
                    params.put("surely", mCertificationMoneyInfo.value.toString())
                    params.put("anxieties", mCertificationOrderInfo.value.toString())
                    ApiClient.instance.getApiService().buyProInfo(params)
                }

                var applyPro = applyNet.await()
                applyPro?.let {
                    if (applyPro.code.isNetSuccess()) {
                        var bundle = Bundle()
                        bundle.putString("urlPath", applyPro.data.tisd)
                        JumpActivityEvent(PathContants.WEB_CONTAINER_ACTIVITY, bundle).publish()
                        finishView()
                    }
                }


            } catch (e: Exception) {
                Log.i(TAG, "Exception: ")
            }
        }
    }


    var TAG = "CertificationListViewModel"
}