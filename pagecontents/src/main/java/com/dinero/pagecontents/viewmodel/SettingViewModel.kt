package com.dinero.pagecontents.viewmodel;

import androidx.lifecycle.MutableLiveData
import com.android.basekt.viewmodel.BaseViewModel
import com.blankj.utilcode.util.AppUtils

class SettingViewModel : BaseViewModel() {

    var mAppVersion = MutableLiveData<String>("v${AppUtils.getAppVersionName()}")

    override var mTitleContent: MutableLiveData<String>
        get() = MutableLiveData<String>("Configuración")
        set(value) {

        }

}