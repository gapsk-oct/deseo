package com.dinero.pagecontents.viewmodel;

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.basekt.base.BaseViewEventConstants
import com.android.basekt.viewmodel.BaseViewModel
import com.dinero.pagecontents.R
import com.dinero.pagecontents.bean.MineDataBean
import kotlinx.coroutines.launch

class MineViewModel : BaseViewModel() {

    val mMineDatas = MutableLiveData<MutableList<MineDataBean>>()

    fun getMineDatas() {
        viewModelScope.launch {
            BaseViewEventConstants.DISMISS_VIEW.publish()
            var datas = arrayListOf<MineDataBean>()
            datas.add(MineDataBean(R.mipmap.ic_setting, "Configuración"))
            mMineDatas.value = datas
        }
    }
}