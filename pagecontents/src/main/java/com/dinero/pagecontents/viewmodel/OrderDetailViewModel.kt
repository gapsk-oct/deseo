package com.dinero.pagecontents.viewmodel;

import androidx.collection.arrayMapOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.basekt.base.BaseViewEventConstants
import com.android.basekt.viewmodel.BaseViewModel
import com.dinero.pagecontents.bean.BaseBean
import com.dinero.pagecontents.bean.OrderDataBean
import com.dinero.pagecontents.bean.OrderDetailDataBean
import com.dinero.pagecontents.net.ApiClient
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class OrderDetailViewModel : BaseViewModel() {
    var mOrderDetailDatas = MutableLiveData<MutableList<OrderDetailDataBean>>()
    override var mTitleContent: MutableLiveData<String>
        get() = MutableLiveData<String>("Configuración")
        set(value) {
        }

    var mCurLoadPage = 1
    fun loadMoreData(type: String) {
        mCurLoadPage++
        getData(type)
    }

    fun getData(type: String, isShowLoading: Boolean = true) {
        viewModelScope.launch {

            if (isShowLoading) {
                BaseViewEventConstants.SHOW_LOADING_VIEW.publish()
            }

            var net = netApi {
                var params = arrayMapOf<String, String>()
                params.put("specks", type)
                params.put("type", "10")
                params.put("actual", mCurLoadPage.toString())
                ApiClient.instance.getApiService().getOrderDetail(params)
            }
            var data = net.await()

            var datas = arrayListOf<OrderDetailDataBean>()
            for (index in 0..10) {
                var data = OrderDetailDataBean("")
                datas.add(data)
            }
            mOrderDetailDatas.value = datas

            if (isShowLoading) {
                BaseViewEventConstants.DISMISS_VIEW.publish()
            }

        }
    }
}