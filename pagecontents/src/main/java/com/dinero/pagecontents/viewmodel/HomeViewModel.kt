package com.dinero.pagecontents.viewmodel;

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.android.basekt.base.BaseViewEventConstants
import com.android.basekt.base.JumpActivityEvent
import com.android.basekt.viewmodel.BaseViewModel
import com.blankj.utilcode.util.NetworkUtils
import com.blankj.utilcode.util.ToastUtils
import com.dinero.pagecontents.bean.*
import com.dinero.pagecontents.net.ApiClient
import com.dinero.pagecontents.util.PathContants
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class HomeViewModel : BaseViewModel() {
    val mHomeDatas = MutableLiveData<HomeDataBean>()

    //大卡位 SSKD
    val mHomeSSKDDatas = MutableLiveData<Direction>()

    //广告图 QQLP
    val mHomeQQLPDDatas = MutableLiveData<Direction>()

    fun getHomeDatas() {
        viewModelScope.launch {
            try {
                if (!NetworkUtils.isConnected()) {
                    ToastUtils.showShort("No internet")
                    return@launch
                }
                BaseViewEventConstants.SHOW_LOADING_VIEW.publish()
                var homebean = netApi {
                    ApiClient.instance.getApiService().getHome()
                }
                var bean = homebean.await()
                bean?.let {
                    Log.i(TAG, "bean:${bean} ")
                    if (bean.code.isNetSuccess()) {
                        bean.data.godf.forEach {
                            if (it.oxford == "SSKD") {
                                Log.i(TAG, "SSKD: ")
                                mHomeSSKDDatas.value = it.direction
                            } else if (it.oxford == "QQLP") {
                                Log.i(TAG, "QQLP: ")
                                mHomeQQLPDDatas.value = it.direction
                            }
                        }
                        mHomeDatas.value = bean.data
                    } else {
                        ToastUtils.showShort(bean.message)
                    }
                }

                BaseViewEventConstants.DISMISS_VIEW.publish()
            } catch (e: Exception) {
                BaseViewEventConstants.DISMISS_VIEW.publish()
                Log.i(TAG, "Exception: ${e.toString()}")
            }
        }
    }

    var TAG = "HOMEMODEL"
    fun goToCertification(proApply: ProApplyDataBean) {
        if (!proApply.tisd.isNullOrEmpty() &&
            (proApply.tisd.contains("http") || proApply.tisd.contains("www"))
        ) {
            var bundle = Bundle()
            bundle.putString("urlPath", proApply.tisd)
            JumpActivityEvent(PathContants.WEB_CONTAINER_ACTIVITY, bundle).publish()
        } else {
            mHomeSSKDDatas.value?.let {
                var bundle = Bundle()
                bundle.putString("proId", it.overa)
                JumpActivityEvent(PathContants.CERTIFICATION_ACTIVITY, bundle).publish()
            }
        }
    }

    var topMouldeId = ""//模块id
    var topLocal = ""//位置

    fun checkProStatus() {
        var topMouldeId = "1001"//模块id
        var topLocal = "0"//位置
        Log.i(TAG, "checkProStatus: ")
        mHomeSSKDDatas.value?.let {
            var proId = it.overa
            if (!NetworkUtils.isConnected()) {
                ToastUtils.showShort("No internet")
                return
            }
            viewModelScope.launch {
                try {
                    BaseViewEventConstants.SHOW_LOADING_VIEW.publish()
                    var applyNet = netApi {
                        var params = hashMapOf<String, String>()
                        params.put("land", topMouldeId)
                        params.put("laws", topLocal)
                        params.put("heartily35", proId)
                        ApiClient.instance.getApiService().getProInfo(params)
                    }
                    var applyPro = applyNet.await()

                    BaseViewEventConstants.DISMISS_VIEW.publish()

                    applyPro?.let {
                        Log.i(TAG, "applyPro: ${applyPro}")
                        if (applyPro.code.isNetSuccess()) {
                            goToCertification(applyPro.data)
                        } else {
                            ToastUtils.showShort(applyPro.message)
                            if (applyPro.code.isNeedLogin()) {
                                mViewEvents.value = JumpActivityEvent(PathContants.LOGIN_ACTIVITY)
                                finishView()
                            }
                        }
                    }

                } catch (e: java.lang.Exception) {
                    BaseViewEventConstants.DISMISS_VIEW.publish()
                }
            }
        }
    }
}