package com.dinero.pagecontents.viewmodel;

import androidx.lifecycle.MutableLiveData
import com.android.basekt.viewmodel.BaseViewModel

class WebContainerViewModel : BaseViewModel() {

    var finishView = MutableLiveData<Boolean>()
    override fun finishView() {
        finishView.value = true
    }

}