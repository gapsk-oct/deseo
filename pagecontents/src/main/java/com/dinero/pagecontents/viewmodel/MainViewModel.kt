package com.dinero.pagecontents.viewmodel

import androidx.lifecycle.MutableLiveData
import com.android.basekt.viewmodel.BaseViewModel

class MainViewModel : BaseViewModel() {


    val mShowView = MutableLiveData<Int>(0)

    fun showHome() {
        if (mShowView.value != 1) {
            mShowView.value = 1
        }
    }

    fun showMine() {
        if (mShowView.value != 2) {
            mShowView.value = 2
        }
    }
}