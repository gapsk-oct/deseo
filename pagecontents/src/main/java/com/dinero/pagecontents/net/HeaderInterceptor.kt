package com.dinero.deseo.net

import android.os.Build
import android.util.Log
import androidx.collection.arrayMapOf
import com.blankj.utilcode.util.AppUtils
import com.blankj.utilcode.util.RomUtils
import com.dinero.pagecontents.util.UserInfoUtils
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {


        var request = chain.request()

        var urlPath = request.url.encodedPath

        var builder = request.url.newBuilder()

        addCommonParams().forEach {
            if (!urlPath.contains(it.key)) {
                builder.addQueryParameter(it.key, it.value)
            }
        }
//
//        if (!urlPath.contains("least")) {
//            builder.addQueryParameter("least", "android")
//        }
//        if (!urlPath.contains("nature")) {
//            builder.addQueryParameter("nature", AppUtils.getAppVersionName())
//        }
//        if (!urlPath.contains("perhaps")) {
//            //设备ID
//            builder.addQueryParameter("perhaps", "")
//        }
//
//        if (!urlPath.contains("imitators")) {      //设备os版本，例如：11.2/8.0.0
//            builder.addQueryParameter("imitators", Build.VERSION.RELEASE)
//        }
//
//        if (!urlPath.contains("works")) {
//            builder.addQueryParameter("works", "prestafast-pf")
//        }
//        if (!urlPath.contains("radcliffe")) {
//            //SessionId
//            builder.addQueryParameter("radcliffe", UserInfoUtils.mSerionId)
//        }
//
//        if (!urlPath.contains("mobilePhone")) {    //手机号
//            builder.addQueryParameter("mobilePhone", UserInfoUtils.mPhoneNum)
//        }
//
//        if (!urlPath.contains("charming")) {
//            //gps_adid
//            builder.addQueryParameter("charming", "")
//        }
//
//        if (!urlPath.contains("tablecloth")) {       //app标识Z
//            builder.addQueryParameter("tablecloth", "presta_fast")
//        }
//        if (!urlPath.contains("human")) {
//            RomUtils.getRomInfo().name?.let {
//                //设备名称，例如：iphoneX
//                builder.addQueryParameter("human", it)
//            }
//        } else {
//            builder.addQueryParameter("human", "")
//        }

        request = request.newBuilder().url(builder.build()).build()

        return chain.proceed(request)
    }
}

fun String.addPathCommonParams(): String {
    var params: Map<String, String> = addCommonParams()
    var paramsPath = this
    params.forEach {
        paramsPath +=
            if (paramsPath.contains("?")) {
                if (!this.contains("${it.key}=")) "&${it.key}=${it.value}" else ""
            } else {
                "?${it.key}=${it.value}"
            }
    }
    return paramsPath
}

fun addCommonParams(): Map<String, String> {
    var map = arrayMapOf<String, String>()
    map.put("least", "android")
    map.put("nature", AppUtils.getAppVersionName())
    //设备名称，例如：iphoneX
    try {
        var romInfo = RomUtils.getRomInfo()
        if (romInfo != null && !romInfo.name.isNullOrEmpty()) {
            map.put("human", romInfo.name)
        } else {
            map.put("human", "")
        }
    } catch (e: Exception) {
        map.put("human", "")
        Log.i("CommonParams", "addCommonParams:===>${e.toString()} ")
    }
    //设备ID
    map.put("perhaps", "")
    //设备os版本，例如：11.2/8.0.0
    map.put("imitators", Build.VERSION.RELEASE)
    //市场
    map.put("works", "prestafast-pf")
    //SessionId
    map.put("radcliffe", UserInfoUtils.getSerionId())
    //手机号
    map.put("mobilePhone", UserInfoUtils.getPhone())
    //gps_adid
    map.put("charming", "")
    //app标识Z
    map.put("tablecloth", "presta_fast")
    return map
}
