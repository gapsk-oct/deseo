package com.dinero.pagecontents.net

import com.dinero.deseo.net.HeaderInterceptor
import okhttp3.Interceptor

class ApiClient private constructor() : INetApi() {

    companion object {
        val instance: ApiClient by lazy {
            ApiClient()
        }
    }

    fun getApiService(): ApiService {
        return getRetrofit(ApiService::class.java).create(ApiService::class.java)
    }

    override fun getBaseUrl(): String {
        return "http://47.89.233.2:8101/"
    }

    override fun getInterceptor(): ArrayList<Interceptor>? {
        var array = arrayListOf<Interceptor>()
//        array.add(NetInterceptor())
        array.add(HeaderInterceptor())
        return array
    }
}

