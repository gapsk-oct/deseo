package com.dinero.pagecontents.net

import com.dinero.pagecontents.bean.*

import com.dinero.pagecontents.bean.AgreeDataBean
import com.dinero.pagecontents.bean.BaseBean
import com.dinero.pagecontents.bean.CertificationDataBean
import io.reactivex.Observable
import retrofit2.http.*

/**
 * http://47.103.73.105:8090/mexicodeseo/
 * http://47.103.73.105:8090/mexico
 *
 * 899654321   202108
 */
interface ApiService {
    @GET("Api/member/bindingcode")
    suspend fun bindingCode(@QueryMap map: Map<String, String>): Observable<String>

    @GET("api/member/studio")
    suspend fun login(@QueryMap map: Map<String, String>): BaseBean<Any>

    @POST("/Pklod/sendSmscode")
    @FormUrlEncoded
    suspend fun getSmsCode(@Field("represented") phone: String): BaseBean<LoginSmsCode>

    @POST("/Pklod/sendVoicecode")
    @FormUrlEncoded
    suspend fun getVoiceCode(@Field("represented") phone: String): BaseBean<LoginSmsCode>

    @POST("/Pklod/login")
    @FormUrlEncoded
    suspend fun smsLogin(
        @Field("alps") phone: String,
        @Field("england") england: String
    ): BaseBean<PersonInfo>

    @POST("/Qulof/detail")
    @FormUrlEncoded
    suspend fun getProDetail(
        @FieldMap map: Map<String, String>,

        ): BaseBean<CertificationDataBean>

    @GET("/v3/contract/contract-jump-api")
    suspend fun getAgreeLink(
        @QueryMap map: Map<String, String>,
    ): BaseBean<AgreeDataBean>

    @POST("/Qulof/apply")
    @FormUrlEncoded
    suspend fun getProInfo(
        @FieldMap map: Map<String, String>,

        ): BaseBean<ProApplyDataBean>

    @POST("/Qulof/jump")
    @FormUrlEncoded
    suspend fun buyProInfo(
        @FieldMap map: Map<String, String>,

        ): BaseBean<ProApplyDataBean>

    @GET("/Qulof/index")
    suspend fun getHome(): BaseBean<HomeDataBean>

    @GET("/Qulof/survey")
    suspend fun getInformation(): BaseBean<CertifyInformationDataBean>

    @POST("/Qulof/closeUser")
    @FormUrlEncoded
    suspend fun getCertifyContactInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<CertifyContactDataBean>

    @POST("/Qulof/personalMaterial")
    @FormUrlEncoded
    suspend fun getCertifyPersonInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<CertifyInformationDataBean>

    @POST("/Qulof/cardInit")
    @FormUrlEncoded
    suspend fun getCertifyBankInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<CertifyInformationDataBean>

    @POST("/Qulof/jobInit")
    @FormUrlEncoded
    suspend fun getCertifyWorkInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<CertifyInformationDataBean>


    @POST("/LikeMd/savePersonalMaterial")
    @FormUrlEncoded
    suspend fun submitCertifyPersonInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<Any>

    @POST("/Qulof/SaveJob")
    @FormUrlEncoded
    suspend fun submitCertifyWorkInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<Any>

    @POST("/Qulof/SaveCard")
    @FormUrlEncoded
    suspend fun submitCertifyBankInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<Any>


    @POST("/Qulof/SaveCloseUser")
    @FormUrlEncoded
    suspend fun submitCertifyContactInformation(
        @FieldMap map: Map<String, String>,

        ): BaseBean<Any>

    @POST("/Qulof/orderIndex")
    @FormUrlEncoded
    suspend fun getOrderDetail(
        @FieldMap map: Map<String, String>,
    ): BaseBean<OrderDataBean>

}