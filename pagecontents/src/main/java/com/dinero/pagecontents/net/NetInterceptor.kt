package com.dinero.deseo.net

import com.blankj.utilcode.util.NetworkUtils
import okhttp3.Interceptor
import okhttp3.Response
import java.lang.RuntimeException

class NetInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        if (NetworkUtils.isConnected()) {
            return chain.proceed(chain.request())
        } else {
            throw RuntimeException("No internet")
        }
    }
}