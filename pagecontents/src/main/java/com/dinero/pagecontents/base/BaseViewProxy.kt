package com.dinero.pagecontents.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.android.basekt.base.BaseViewProxy
import com.dinero.pagecontents.util.PathContants
import com.dinero.pagecontents.view.LoadingDialog
import com.dinero.pagecontents.activity.*

class BaseViewProxy(context: Context) : BaseViewProxy(context) {


    val loadingDialog: LoadingDialog by lazy {
        LoadingDialog(mContext)
    }
    val TAG = "BaseViewProxy"

    /**
     * 取消状态界面
     */
    override fun dismissView() {
        if (loadingDialog.isShowing) {
            loadingDialog.dismiss()
        }
        Log.i(TAG, "dismissView: ")
    }

    /**
     * 显示空界面
     */
    override fun showEmptyView() {
        loadingDialog.show()
    }

    /**
     * 显示loading
     */
    override fun showLoadingView() {
        if (!loadingDialog.isShowing) {
            loadingDialog.show()
        }
        Log.i(TAG, "showLoadingView: ")
    }

    /**
     * 跳转界面
     *
     * @param path
     * @param bundle
     */
    override fun jumpPage(path: String, bundle: Bundle?) {
        var intent: Intent? = null
        when (path) {
            PathContants.LOGIN_ACTIVITY -> {
                intent = Intent(mContext, LoginActivity::class.java)
            }
            PathContants.MAIN_ACTIVITY -> {
                intent = Intent(mContext, MainActivity::class.java)
            }
            PathContants.CERTIFICATION_ACTIVITY -> {
                intent = Intent(mContext, CertificationListActivity::class.java)
            }
            PathContants.CERTIFY_PERSON_INFORMATION_ACTIVITY -> {
                intent = Intent(mContext, CertifyPersonInformationActivity::class.java)
            }

            PathContants.CERTIFY_WORK_INFORMATION_ACTIVITY -> {
                intent = Intent(mContext, CertifyWorkInfomationActivity::class.java)
            }
            PathContants.CERTIFY_CONTACT_INFORMATION_ACTIVITY -> {
                intent = Intent(mContext, CertifyContactInformationActivity::class.java)
            }

            PathContants.CERTIFY_BANK_INFORMATION_ACTIVITY -> {
                intent = Intent(mContext, CertifyBankInfomationActivity::class.java)
            }

            PathContants.WEB_CONTAINER_ACTIVITY -> {
                intent = Intent(mContext, WebContainerActivity::class.java)
            }

        }

        Log.i(TAG, "intent: ${intent}")
        intent?.let {
            bundle?.let {
                intent.putExtras(bundle)
            }
            mContext.startActivity(intent)
        }

    }

    override fun finishView() {
        if (loadingDialog.isShowing) {
            loadingDialog.dismiss()
        }

        (mContext as Activity).finish()
    }

    /**
     * 显示重新刷新界面
     */
    override fun showReloadView() {
    }


}