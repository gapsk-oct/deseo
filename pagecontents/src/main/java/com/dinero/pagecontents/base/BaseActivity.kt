package com.dinero.pagecontents.base
import android.os.Bundle
import androidx.databinding.ViewDataBinding
import com.android.basekt.activity.BaseDataBindingActivity
import com.android.basekt.viewmodel.BaseViewModel
import com.blankj.utilcode.util.BarUtils
import com.dinero.pagecontents.base.BaseViewProxy

abstract class BaseActivity<DB : ViewDataBinding, VM : BaseViewModel> :
    BaseDataBindingActivity<DB, VM>() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(isLightMode()){
            BarUtils.setStatusBarLightMode(this, true)
        }
        setBaseViewProxy(BaseViewProxy(this))
    }

    open fun isLightMode(): Boolean {
        return true
    }
}