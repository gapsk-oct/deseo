package com.dinero.pagecontents.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import com.android.basekt.fragment.BaseDataBindFragment
import com.android.basekt.viewmodel.BaseViewModel

abstract class BaseFragment<DB : ViewDataBinding, VM : BaseViewModel> :
    BaseDataBindFragment<DB, VM>() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setBaseViewProxy(context?.let { BaseViewProxy(it) })
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}