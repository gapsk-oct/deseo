package com.android.basekt.base

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModel
import com.android.basekt.viewmodel.BaseViewModel
import kotlinx.coroutines.CoroutineScope

abstract class ViewEvent {
    var handled = false
}

/**
 * 基础事件
 */
class SimpleViewEvent(
    val event: Int
) : ViewEvent()

/**
 * 界面跳转事件
 */
class JumpActivityEvent(
    val className: String,
    val bundle: Bundle? = null
) : ViewEvent()

