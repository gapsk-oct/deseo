package com.android.basekt.base

import android.util.SparseArray
import androidx.annotation.LayoutRes

class BaseDataBindingConfig(
    @param:LayoutRes val layoutId: Int
) {
    /**
     * 获取bingding参数
     *
     * @return
     */
    val bindingParams: SparseArray<*> = SparseArray<Any>()

    fun addBindingParam(variableId: Int, value: Any): BaseDataBindingConfig {
        if (variableId > 0 && value != null) {
            bindingParams.put(variableId, value as Nothing?)
        }
        return this
    }

}