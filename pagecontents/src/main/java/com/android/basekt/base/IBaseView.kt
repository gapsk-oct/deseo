package com.android.basekt.base

import android.os.Bundle

interface IBaseView {

    fun initView()

    fun initData()

    /**
     * 显示loading
     */
    fun showLoadingView()

    /**
     * 取消状态界面
     */
    fun dismissView()

    /**
     * 显示空界面
     */
    fun showEmptyView()

    /**
     * 显示重新刷新界面
     */
    fun showReloadView()


    /**
     * 跳转界面
     *
     * @param path
     * @param bundle
     */
    fun jumpPage(path: String, bundle: Bundle? = null)

    fun finishView()
}