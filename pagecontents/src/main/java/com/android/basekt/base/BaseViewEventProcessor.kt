package com.android.basekt.base

import android.util.Log


fun IBaseView.handlerEventObserver(viewEvent: ViewEvent) {
    when (viewEvent) {
        is SimpleViewEvent -> {
            when (viewEvent.event) {
                BaseViewEventConstants.FINISH_VIEW -> finishView()
                BaseViewEventConstants.SHOW_LOADING_VIEW -> showLoadingView()
                BaseViewEventConstants.DISMISS_VIEW -> dismissView()
                BaseViewEventConstants.SHOW_EMPTY_VIEW -> showEmptyView()
            }
        }
        is JumpActivityEvent -> {
            jumpPage(viewEvent.className, viewEvent.bundle)
        }
    }
}


//class BaseViewEventProcessor(
//    t: ViewEvent,
//    iBaseView: IBaseView
//) {
//    /**
//     * 注册IBaseView 事
//     *
//     * @param viewModel
//     * @param iBaseView
//     */
//    private fun registEvent(t: T, viewModel: BaseViewModel?, iBaseView: IBaseView) {
//        if (viewModel != null) {
//            /**
//             * 通知IBaseView loading
//             */
//            viewModel.getBaseViewEventLiveData().getShowLoadingEvent().observe(t,
//                Observer<Boolean> { aBoolean ->
//                    if (aBoolean) {
//                        iBaseView.showLoadingView()
//                    }
//                })
//            /**
//             * 通知IBaseView 消息loading
//             */
//            viewModel.getBaseViewEventLiveData().getDismissViewEvent().observe(t,
//                Observer<Boolean> { aBoolean ->
//                    if (aBoolean) {
//                        iBaseView.dismissView()
//                    }
//                })
//            /**
//             * 通知IBaseView  显示空界面
//             */
//            viewModel.getBaseViewEventLiveData().getEmptyViewEvent().observe(t,
//                Observer<Boolean> { aBoolean ->
//                    if (aBoolean) {
//                        iBaseView.showEmptyView()
//                    }
//                })
//            /**
//             * 通知IBaseView  显示重新加载界面
//             */
//            viewModel.getBaseViewEventLiveData().getReloadViewEvent().observe(t,
//                Observer<Boolean> { aBoolean ->
//                    if (aBoolean) {
//                        iBaseView.showReloadView()
//                    }
//                })
//            /**
//             * 通知IBaseView  跳转界面
//             */
//            viewModel.getBaseViewEventLiveData().getJumpPagePathEvent().observe(t,
//                Observer<String?> { path -> iBaseView.jumpPage(path!!) })
//            /**
//             * 通知IBaseView  跳转界面
//             */
//            viewModel.getBaseViewEventLiveData().getJumpPageParamsEvent().observe(t,
//                Observer<Bundle> { paramsMap ->
//                    if (paramsMap.containsKey(Constants.JUMP_PAGE_PATH_KEY)) {
//                        val path = paramsMap.getString(Constants.JUMP_PAGE_PATH_KEY)
//                        if (!TextUtils.isEmpty(path)) {
//                            paramsMap.remove(Constants.JUMP_PAGE_PATH_KEY)
//                            iBaseView.jumpPage(path!!, paramsMap)
//                        }
//                    }
//                })
//        }
//    }
//
//    init {
//        registEvent(t, baseViewModel, iBaseView)
//    }
//}
//


