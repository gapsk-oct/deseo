package com.android.basekt.base

object BaseViewEventConstants {
    const val FINISH_VIEW = 1000

    const val SHOW_LOADING_VIEW = 1001

    const val DISMISS_VIEW = 1002

    const val SHOW_EMPTY_VIEW = 1003

    const val JUMP_PAGE = 1004

    const val HIDE_KEYBOARD_VIEW = 1005
}