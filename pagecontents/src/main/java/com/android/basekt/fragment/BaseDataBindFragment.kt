package com.android.basekt.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.basekt.base.*
import com.android.basekt.viewmodel.BaseViewModel


/**
 * @ClassName BaseDataBindFragment
 * @Author hyy
 * @Description 本身不对IBaseView做任何处理，我是中间商，只对相应的事件做事件回调处理，不具体实现对应的事件。
 * 需要注册代理类，处理实现对应的View操作
 * @Date 2021/6/16
 * @Time 7:17 PM
 */
abstract class BaseDataBindFragment<DB : ViewDataBinding, VM : BaseViewModel> :
    Fragment(), IBaseView, IBaseBindingView<VM> {

    lateinit var mBinding: DB

    val mViewModel: VM by lazy {
        getViewModel(initViewModelClazz())
    }


    /**
     * IBaseView 具体的处理代理类
     * 为空的话，走默认BaseDataBindingActivity 实现方法
     */
    private var baseViewProxy: BaseViewProxy? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //获取ViewModel
        initViewModel()
        initBinding(inflater, container)
        // 注册viewmodel 公共事件
        registViewModelEvent()
        initView()
        initData()
        return mBinding!!.root
    }

    private fun initViewModel() {
        mViewModel.mViewEvents.observe(viewLifecycleOwner) {
            handlerEventObserver(it)
        }
        lifecycle.addObserver(mViewModel)
    }

    private fun initBinding(inflater: LayoutInflater, container: ViewGroup?) {
        mBinding =
            DataBindingUtil.inflate(inflater, dataBindingConfig.layoutId, container, false)
        //binding 绑定 lifecycle
        mBinding.lifecycleOwner = this
    }

    /**
     * 注册IBaseView公共事件处理
     */
    private fun registViewModelEvent() {

    }

    private fun getViewModel(modelClass: Class<VM>): VM {
        return ViewModelProvider(this).get(modelClass)
    }

    /**
     * 显示loading
     */
    override fun showLoadingView() {
        baseViewProxy?.showLoadingView()
    }

    /**
     * 取消状态界面
     */
    override fun dismissView() {
        baseViewProxy?.dismissView()
    }

    /**
     * 显示空界面
     */
    override fun showEmptyView() {
        baseViewProxy?.showEmptyView()
    }

    /**
     * 显示重新刷新界面
     */
    override fun showReloadView() {
        baseViewProxy?.showReloadView()
    }

    /**
     * 跳转界面
     *
     * @param path
     * @param bundle
     */
    override fun jumpPage(path: String, bundle: Bundle?) {
        if (baseViewProxy != null) {
            baseViewProxy?.jumpPage(path, bundle)
        }
    }

    override fun finishView() {
        baseViewProxy?.finishView() ?: activity?.finish()
    }

    /**
     * 通过反射拿到，IBaseView 处理代理类
     *
     * @return
     */
    fun getBaseViewProxy(): BaseViewProxy? {
//        val baseViewProxy: BaseViewProxy? = null
//        val clazz: Class<BaseViewProxy> = BaseApplication.getBaseViewProxyClass()
//        if (clazz != null) {
//            try {
//                val constructor: Constructor<BaseViewProxy> = clazz.getConstructor(
//                    Context::class.java
//                )
//                return constructor.newInstance(context)
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
        return baseViewProxy
    }

    /**
     * 手动调用设置代理类
     *
     * @param baseViewProxy
     */
    fun setBaseViewProxy(baseViewProxy: BaseViewProxy?) {
        this.baseViewProxy = baseViewProxy
    }
}
