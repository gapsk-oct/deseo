package com.android.basekt.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.android.basekt.base.*
import com.android.basekt.viewmodel.BaseViewModel
import com.blankj.utilcode.util.KeyboardUtils

/**
 * @ClassName BaseDataBindingActivity
 * @Author hyy
 * @Description 本身不对IBaseView做任何处理，我是中间商，只对相应的事件做事件回调处理，不具体实现对应的事件。
 * 需要注册代理类，处理实现对应的View操作
 * @Date 2021/6/16
 * @Time 7:17 PM
 */
abstract class BaseDataBindingActivity<DB : ViewDataBinding, VM : BaseViewModel> :
    AppCompatActivity(), IBaseView, IBaseBindingView<VM> {
    lateinit var mBinding: DB

    val mViewModel: VM by lazy {
        getViewModel(initViewModelClazz())
    }

    /**
     * IBaseView 具体的处理代理类
     * 为空的话，走默认BaseDataBindingActivity 实现方法
     */
    private var baseViewProxy: BaseViewProxy? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        baseViewProxy = getBaseViewProxy()
        //初始化viewmodel
        initViewModel()
        //初始化dataBinging
        initBinding()
        //注册livedata公共事件
        registViewModelEvent()
        initView()
        initData()
    }

    private fun initViewModel() {

        if (mViewModel != null) {
            lifecycle.addObserver(mViewModel)
        }
    }

    /**
     * 初始化binding
     */
    private fun initBinding() {
        val baseDataBindingConfig: BaseDataBindingConfig = dataBindingConfig
        mBinding = DataBindingUtil.setContentView(this, baseDataBindingConfig.layoutId)
        mBinding.lifecycleOwner = this
        val bindingParams = baseDataBindingConfig.bindingParams
        for (i in 0 until bindingParams.size()) {
            mBinding.setVariable(bindingParams.keyAt(i), bindingParams.valueAt(i))
        }
    }

    private fun getViewModel(modelClass: java.lang.Class<VM>): VM {
        var vm = ViewModelProvider(this).get(modelClass)
        vm.mViewEvents.observe(this) {
            handlerEventObserver(it)
            when (it) {
                is SimpleViewEvent -> {
                    when (it.event) {
                        BaseViewEventConstants.HIDE_KEYBOARD_VIEW -> {
                            KeyboardUtils.hideSoftInput(this)
                        }
                    }
                }
            }

        }
        return vm
    }

    /**
     * 注册BaseViewModelEvent
     */
    private fun registViewModelEvent() {

    }

    /**
     * 显示loading
     */
    override fun showLoadingView() {
        if (baseViewProxy != null) {
            baseViewProxy?.showLoadingView()
        }
    }

    /**
     * 取消状态界面
     */
    override fun dismissView() {
        if (baseViewProxy != null) {
            baseViewProxy?.dismissView()
        }
    }

    /**
     * 显示空界面
     */
    override fun showEmptyView() {
        if (baseViewProxy != null) {
            baseViewProxy?.showEmptyView()
        }
    }

    /**
     * 显示重新刷新界面
     */
    override fun showReloadView() {
        if (baseViewProxy != null) {
            baseViewProxy?.showReloadView()
        }
    }

    /**
     * 跳转界面
     *
     * @param path
     * @param bundle
     */
    override fun jumpPage(path: String, bundle: Bundle?) {
        if (baseViewProxy != null) {
            baseViewProxy?.jumpPage(path, bundle)
        }
    }

    override fun finishView() {
        if (baseViewProxy != null) {
            baseViewProxy?.finishView()
        } else {
            finish()
        }
    }

    /**
     * 通过反射拿到，IBaseView 处理代理类
     *
     * @return
     */
    fun getBaseViewProxy(): BaseViewProxy? {
        val baseViewProxy: BaseViewProxy? = null
        return baseViewProxy
    }

    /**
     * 手动调用设置代理类
     *
     * @param baseViewProxy
     */
    fun setBaseViewProxy(baseViewProxy: BaseViewProxy) {
        this.baseViewProxy = baseViewProxy
    }
}